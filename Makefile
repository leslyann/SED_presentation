TARGET=main

all: pdf

pdf: bibtex
	pdflatex $(TARGET).tex
	pdflatex $(TARGET).tex

pdf-lazy:
	pdflatex $(TARGET).tex

bibtex: pdf-lazy
	biber $(TARGET)

clean:
	rm -f *.dvi *.aux *.bbl *.blg $(TARGET).ps $(TARGET).pdf *.toc *.ind *.out *.brf *.ilg *.idx *.log *.bcf *.nav *.run.xml *.snm *.vrb *.backup tex/*.backup *~

mrproper: clean
	rm -f $(TARGET).pdf
